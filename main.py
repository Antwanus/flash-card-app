from tkinter import *
import pandas
import random

try:
    data = pandas.read_csv(filepath_or_buffer="resources/data_to_learn.csv")
except FileNotFoundError as e:
    print('An error has occurred: ', e)
    original_data = pandas.read_csv(filepath_or_buffer="resources/top_fr_to_eng_data.csv")
    to_learn = original_data.to_dict(orient='records')
else:
    to_learn = data.to_dict(orient='records')

current_card = {}

# ------------------ UPDATE_CARD ------------------------ #
def known_card():
    """removes current card from the dictionary & calls next_card()"""
    to_learn.remove(current_card)
    next_card()

    data_frame = pandas.DataFrame(to_learn)
    data_frame.to_csv(path_or_buf='resources/data_to_learn.csv', index=False)  # This works, but code completion fails..


# ------------------ UPDATE_CARD ------------------------ #
def next_card():
    """cancels the timer, if it's still running, updates UI, gets random choice & calls flip_card()"""
    global current_card, flip_timer
    # Reset timer when a button is clicked
    window.after_cancel(flip_timer)

    current_card = random.choice(to_learn)
    print(f'{current_card["FR"]}    =   {current_card["ENG"]}')
    canvas.itemconfig(card_title, text="French", fill='black')
    canvas.itemconfig(card_word, text=current_card["FR"], fill='black')
    canvas.itemconfig(card, image=CARD_FRONT_IMG)

    flip_timer = window.after(3000, flip_card)


def flip_card():
    """updates UI"""
    canvas.itemconfig(card_title, text="English", fill='white')
    canvas.itemconfig(card_word, text=current_card["ENG"], fill='white')
    canvas.itemconfig(card, image=CARD_BACK_IMG_PATH)


# --------------------- GUI ----------------------------- #
BACKGROUND_COLOR = '#B1DDC6'

window = Tk()
window.title("ENG -> FR")
window.config(padx=50, pady=50, bg=BACKGROUND_COLOR)
CARD_BACK_IMG_PATH = PhotoImage(file='resources/images/card_back.png')
CARD_FRONT_IMG = PhotoImage(file='resources/images/card_front.png')
RIGHT_IMG_PATH = PhotoImage(file='resources/images/right.png')
WRONG_IMG_PATH = PhotoImage(file='resources/images/wrong.png')


canvas = Canvas(width=800, height=600, bg=BACKGROUND_COLOR, highlightthickness=0)
card = canvas.create_image(400, 300, image=CARD_FRONT_IMG)
card_title = canvas.create_text(400, 130, text='', font=('Times new roman', 40, 'italic'))
card_word = canvas.create_text(400, 300, text='', font=('Times new roman', 50, 'bold'))
canvas.grid(row=0, column=0, columnspan=2)

known_word_btn = Button(image=RIGHT_IMG_PATH, highlightthickness=0, border=0, command=known_card)
known_word_btn.grid(row=1, column=1)
unknown_word_btn = Button(image=WRONG_IMG_PATH, highlightthickness=0, border=0, command=next_card)
unknown_word_btn.grid(row=1, column=0)


flip_timer = window.after(3000, flip_card)
next_card()

window.mainloop()
